﻿using Apeseg.Soat.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Apeseg.Soat.Services
{
    public class SoatServices
    {
        private readonly string _subscription = "51becec7dc6a4f53af478a954f808f8d";//PRODUCCIÓN
   
        string url = "https://apesegsoat.azure-api.net/consultasoatprd/api/Busqueda/BuscarCertificadoActualPorPlaca/";//PRODUCCIÓN

        private async Task<Certificado> BuscarCertificado(string Placa)
        {
            Certificado respuestaBase = new Certificado();
            string responseJsonString = null;
            List<Certificado> listaCertificado = new List<Certificado>();
            using (var httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", _subscription);

                    string URI = url + Placa;

                    HttpResponseMessage response = await httpClient.GetAsync(URI);
                    
                    if (response.IsSuccessStatusCode)
                    {
                        HttpContent content = response.Content;
                        responseJsonString = await content.ReadAsStringAsync();
                        respuestaBase = JsonConvert.DeserializeObject<Certificado>(responseJsonString);

                        if (respuestaBase==null)
                        {
                            respuestaBase = new Certificado();
                            respuestaBase.Existe = false;
                        }
                        else
                        {
                            respuestaBase.Existe = true;
                   
                        }
                        
                        return respuestaBase;
                    }
                    else
                    {
                        respuestaBase = new Certificado();
                        respuestaBase.Existe = false;
                        respuestaBase.ErrorConexion = false;
                        return respuestaBase;
                    }
                }
                catch (Exception ex)
                {
                    respuestaBase.ErrorConexion = true;
                    return respuestaBase;
                }
            }


        }

        public async Task<Certificado> BuscarCertificadoPorPlaca(string Placa)
        {
            Certificado Certificado = await BuscarCertificado(Placa);
            return Certificado;
        }
    }
}
