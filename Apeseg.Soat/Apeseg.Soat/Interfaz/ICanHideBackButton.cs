﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apeseg.Soat.Interfaz
{
    public interface ICanHideBackButton
    {
        bool HideBackButton { get; set; }
    }
}
