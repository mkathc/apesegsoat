﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apeseg.Soat.Model
{
    public class Certificado
    {
        public string NombreCompania { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string Placa { get; set; }
        public string NumeroPoliza { get; set; }
        public string NombreUsoVehiculo { get; set; }
        public string NombreClaseVehiculo { get; set; }
        public string Estado { get; set; }
        public string CodigoUnicoPoliza { get; set; }
        public string CodigoSBSAseguradora { get; set; }
        public string FechaControlPolicial { get; set; }
        public string NombreContratante { get; set; }
        public string NombreUbigeo { get; set; }
        public string NumeroSerieMotor { get; set; }
        public string NumeroSerieChasis { get; set; }

        public bool  ErrorConexion { get; set; }
        public bool  Existe { get; set; }

        public string NumeroAseguradora { get; set; }

        public string TipoCertificado { get; set; }
    }
}
