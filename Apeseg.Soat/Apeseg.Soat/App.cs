﻿using Apeseg.Soat.Views;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Apeseg.Soat
{
    public class App : Application
    {
        public App()
        {
            Xamarin.Forms.Device.OnPlatform(iOS: () => { NavigationPage.SetBackButtonTitle(this, "Atras"); });
            MainPage = new NavigationPage(new ConsultaSoat())
            {
                BarTextColor = Color.White,
                BarBackgroundColor = Color.FromHex("#401364")
            };
        }
        protected override void OnStart()
        {
            AppCenter.Start("android=5590634e-f608-4fc4-bab6-4e18e1c7dba9;" + "uwp={Your UWP App secret here};" + "ios=209ae11a-fc4b-4942-a503-5f00b2030247", typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
