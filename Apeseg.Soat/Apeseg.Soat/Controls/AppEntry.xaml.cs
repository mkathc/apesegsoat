﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Apeseg.Soat.Controls
{
    public partial class AppEntry : ContentView
    {
        public AppEntry()
        {
            InitializeComponent();
        }

        public new double WidthRequest
        {
            get { return Layout.WidthRequest; }
            set
            {
                Layout.WidthRequest = value;
            }
        }

        public string Text
        {
            get { return Entry.Text; }
            set
            {
                Entry.Text = value;
            }
        }

        public string Placeholder
        {
            get { return Entry.Placeholder; }
            set
            {
                Entry.Placeholder = value;
            }
        }

        public bool IsPassword
        {
            get { return Entry.IsPassword; }
            set
            {
                Entry.IsPassword = value;
            }
        }

        public ImageSource ImageSource
        {
            get { return Image.Source; }
            set
            {
                Image.Source = value;
            }
        }
    }
}
