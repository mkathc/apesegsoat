﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Apeseg.Soat.Controls
{
    public partial class MessageBox : ContentView
    {
        public static MessageBox Current;
        public static void Show(Grid layout, string message, string primaryImage = "ic_info.png", string secondaryImage = null, MessageBoxOptions messageBoxOptions = MessageBoxOptions.Ok, EventHandler onOk = null, EventHandler onCancel = null)
        {
            if (null == Current)
            {
                Current = new MessageBox(layout, message, primaryImage, secondaryImage, messageBoxOptions, onOk, onCancel);

                if (layout.ColumnDefinitions.Count > 0)
                    Grid.SetColumnSpan(Current, layout.ColumnDefinitions.Count);

                if (layout.RowDefinitions.Count > 0)
                    Grid.SetRowSpan(Current, layout.RowDefinitions.Count);

                layout.Children.Add(Current);
            }
        }

        private readonly Layout<View> _layout;

        private MessageBox(Layout<View> layout, string message, string primaryImage, string secondaryImage, MessageBoxOptions messageBoxOptions, EventHandler onOk, EventHandler onCancel)
        {
            InitializeComponent();
            _layout = layout;

            MessageLabel.Text = message;

            OkButton.Text = "ACEPTAR";
            OkButton.Clicked += (s, a) =>
            {
                Dismiss();

                if (null != onOk)
                    onOk(this, EventArgs.Empty);
            };

            if (!string.IsNullOrWhiteSpace(primaryImage))
                PrimaryImage.Source = Device.OnPlatform(null, primaryImage, null);

            if (!string.IsNullOrWhiteSpace(secondaryImage))
                SecondaryImage.Source = Device.OnPlatform(null, secondaryImage, null);

            if (messageBoxOptions == MessageBoxOptions.Ok)
            {
                Grid.SetColumnSpan(OkButton, 3);
                OkButton.HorizontalOptions = LayoutOptions.Center;
                OkButton.WidthRequest = 210;
                CancelButton.IsVisible = false;
            }
            else
            {
                CancelButton.Clicked += (s, a) =>
                {
                    Dismiss();

                    if (null != onCancel)
                        onCancel(this, EventArgs.Empty);
                };
                CancelButton.Text = "CANCELAR";
            }
        }

        private void Dismiss()
        {
            _layout.Children.Remove(this);


            Current = null;

        }

    }

    public enum MessageBoxOptions
    {
        Ok = 1,
        OkCancel = 2
    }
}
