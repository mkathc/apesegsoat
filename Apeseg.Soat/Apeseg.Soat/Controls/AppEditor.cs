﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Apeseg.Soat.Controls
{
    public class AppEditor : View
    {
        public static readonly BindableProperty IsMultilineProperty =
            BindableProperty.Create<AppEditor, bool>(p => p.IsMultiline, false);

        public bool IsMultiline
        {
            get
            {
                return (bool)GetValue(IsMultilineProperty);
            }

            set
            {
                SetValue(IsMultilineProperty, value);
            }
        }

        public static readonly BindableProperty IsPasswordProperty =
            BindableProperty.Create<AppEditor, bool>(p => p.IsPassword, false);

        public bool IsPassword
        {
            get
            {
                return (bool)GetValue(IsPasswordProperty);
            }

            set
            {
                SetValue(IsPasswordProperty, value);
            }
        }

     

      

        public static readonly BindableProperty TextProperty =
            BindableProperty.Create<AppEditor, string>(p => p.Text, string.Empty);

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }

            set
            {
                SetValue(TextProperty, value);
            }
        }

        public static readonly BindableProperty TextColorProperty =
            BindableProperty.Create<AppEditor, Color>(p => p.TextColor, Color.Default);

        public Color TextColor
        {
            get
            {
                return (Color)GetValue(TextColorProperty);
            }

            set
            {
                SetValue(TextColorProperty, value);
            }
        }

        public static readonly BindableProperty MaxLengthProperty =
            BindableProperty.Create<AppEditor, int>(p => p.MaxLength, 0);

        public int MaxLength
        {
            get
            {
                return (int)GetValue(MaxLengthProperty);
            }

            set
            {
                SetValue(MaxLengthProperty, value);
            }
        }

        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create<AppEditor, string>(p => p.Placeholder, string.Empty);

        public string Placeholder
        {
            get
            {
                return (string)GetValue(PlaceholderProperty);
            }

            set
            {
                SetValue(PlaceholderProperty, value);
            }
        }

        public static readonly BindableProperty MinLinesProperty =
            BindableProperty.Create<AppEditor, int>(p => p.MinLines, 1);

        public int MinLines
        {
            get
            {
                return (int)GetValue(MinLinesProperty);
            }

            set
            {
                SetValue(MinLinesProperty, value);
            }
        }
    }
}
