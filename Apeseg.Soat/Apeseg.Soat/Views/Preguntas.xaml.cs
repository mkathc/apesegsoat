﻿using Apeseg.Soat.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Apeseg.Soat.Views
{
    public partial class Preguntas : ContentPage, ICanHideBackButton
    {
        public Preguntas()
        {
            HideBackButton = false;
            Device.OnPlatform(Android: () => { NavigationPage.SetTitleIcon(this, "iconApeseg.png"); });
            InitializeComponent();

            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += PreguntaUnoButton_Clicked;
            UnoLabel.GestureRecognizers.Add(tapGestureRecognizer);

            var DosRecognizer = new TapGestureRecognizer();
            DosRecognizer.Tapped += PreguntaDosButton_Clicked;
            DosLabel.GestureRecognizers.Add(DosRecognizer);

            var TresRecognizer = new TapGestureRecognizer();
            TresRecognizer.Tapped += PreguntaTresButton_Clicked;
            TresLabel.GestureRecognizers.Add(TresRecognizer);

            var CuatroRecognizer = new TapGestureRecognizer();
            CuatroRecognizer.Tapped += PreguntaCuatroButton_Clicked;
            Cuatroabel.GestureRecognizers.Add(CuatroRecognizer);

            var CincoRecognizer = new TapGestureRecognizer();
            CincoRecognizer.Tapped += PreguntaCincoButton_Clicked;
            CincoLabel.GestureRecognizers.Add(CincoRecognizer);

            var SeisRecognizer = new TapGestureRecognizer();
            SeisRecognizer.Tapped += PreguntaSeisButton_Clicked;
            SeisLabel.GestureRecognizers.Add(SeisRecognizer);

            //var SieteRecognizer = new TapGestureRecognizer();
            //SieteRecognizer.Tapped += PreguntaSieteButton_Clicked;
            //SieteLabel.GestureRecognizers.Add(SieteRecognizer);

            //var OchoRecognizer = new TapGestureRecognizer();
            //OchoRecognizer.Tapped += PreguntaOchoButton_Clicked;
            //OchoLabel.GestureRecognizers.Add(OchoRecognizer);

            //var NueveRecognizer = new TapGestureRecognizer();
            //SieteRecognizer.Tapped += PreguntaNueveButton_Clicked;
            //NueveLabel.GestureRecognizers.Add(NueveRecognizer);
        }
        public bool HideBackButton { get; set; }
        private void PreguntaUnoButton_Clicked(object sender, EventArgs e)
        {
            
            Navigation.PushAsync(new Respuesta(1), true);
        }

        private void PreguntaDosButton_Clicked(object sender, EventArgs e)
        {

            Navigation.PushAsync(new Respuesta(2), true);
        }

        private void PreguntaTresButton_Clicked(object sender, EventArgs e)
        {

            Navigation.PushAsync(new Respuesta(3), true);
        }

        private void PreguntaCuatroButton_Clicked(object sender, EventArgs e)
        {

            Navigation.PushAsync(new Respuesta(4), true);
        }

        private void PreguntaCincoButton_Clicked(object sender, EventArgs e)
        {

            Navigation.PushAsync(new Respuesta(5), true);
        }

        private void PreguntaSeisButton_Clicked(object sender, EventArgs e)
        {

            Navigation.PushAsync(new Respuesta(6), true);
        }

        //private void PreguntaSieteButton_Clicked(object sender, EventArgs e)
        //{

        //    Navigation.PushAsync(new Respuesta(7), true);
        //}

        //private void PreguntaOchoButton_Clicked(object sender, EventArgs e)
        //{

        //    Navigation.PushAsync(new Respuesta(8), true);
        //}

        //private void PreguntaNueveButton_Clicked(object sender, EventArgs e)
        //{

        //    Navigation.PushAsync(new Respuesta(9), true);
        //}

    }
}
