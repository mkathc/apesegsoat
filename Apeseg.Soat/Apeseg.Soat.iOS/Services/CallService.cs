﻿using System.Diagnostics;
using System.Text.RegularExpressions;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Apeseg.Soat.iOS.Services;
using Apeseg.Soat.Interfaz;

[assembly: Dependency(typeof(CallService))]
namespace Apeseg.Soat.iOS.Services
{
    public class CallService : ICallService
    {
        public static void Init() { }

        public void MakeCall(string phone)
        {
            try
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(phone, "^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*$"))
                {
                    var url = new NSUrl(string.Format("tel://{0}", phone));
                    UIApplication.SharedApplication.OpenUrl(url);

                    //NSUrl.FromString
                }
                else
                {

                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
            
        }
    }
}
